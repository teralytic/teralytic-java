swagger: "2.0"
info:
  version: "1.0.6"
  title: "teralytic"
  description: |
    The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.
    
    For sandbox testing you may use the api key: `swagger.teralytic.io`
  license:
    name: MIT
    url: https://github.com/apiaryio/teralytic-api/blob/master/LICENSE
    
host: api.teralytic.io
basePath: /v1
consumes:
- application/json
produces:
- application/json

securityDefinitions:
  OAuth2:
    type: oauth2
    flow: application
    tokenUrl: https://auth.teralytic.io/oauth2/token
    scopes:
      https://api.teralytic.io/organization.read: Read organization properties
      https://api.teralytic.io/organization.write: Write organization properties
      https://api.teralytic.io/organization.admin: Administer organization properties
      https://api.teralytic.io/reading.read: Query readings scope
  ApiKeyAuth:
    type: apiKey
    in: header
    name: x-api-key
    
paths:
  /organizations:
    get:
      summary: List all Organizations
      description: Lists all organization, requires admin scope
      operationId: organizationList
      tags: [organization]
      responses:
        200:
          description: Success
          schema:
            type: array
            items:
              $ref: '#/definitions/Organization'
        401:
          $ref: '#/responses/Unauthorized'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
    get:
      summary: Get a specific organization
      description: List single Organization details associated with Organization id provided
      operationId: organizationGet
      tags: [organization]
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Organization'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}/applications:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization for the operation
        type: string
        format: uuid
    get:
      summary: Get organization applications
      description: List all applications for an organization
      operationId: applicationList
      tags: [organization]
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Application'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          - https://api.teralytic.io/organization.admin
          ApiKeyAuth: []
    post:
      summary: Create a new organization client with the specified scope
      description: Create a new third-party application client
      operationId: applicationCreate
      tags: [organization]
      parameters:
        - name: application
          description: The application to create
          in: body
          schema:
            $ref: "#/definitions/Application"
      responses:
        201:
          description: Created
          schema:
            $ref: '#/definitions/Application'
        400:
          $ref: '#/responses/BadRequest'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.write
          - https://api.teralytic.io/organization.admin
          ApiKeyAuth: []
          
  /organizations/{organization_id}/applications/{application_id}:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization for the operation
        type: string
        format: uuid
      - name: application_id
        in: path
        required: true
        description: id of application to delete
        type: string
        format: uuid
    delete:
      summary: Delete an application api client
      description: Delete an application client for the organization from the database
      operationId: applicationDelete
      tags: [organization]
      responses:
        204:
          description: No Content
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          - https://api.teralytic.io/organization.admin
          ApiKeyAuth: []
          
  /organizations/{organization_id}/fields:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
    get:
      summary: List all Fields associated with an organization
      operationId: fieldList
      tags: [organization]
      parameters: 
        - name: points
          description: Array of points (lat,lng) describing a polygon search pattern.
          in: query
          required: false
          minItems: 3
          uniqueItems: true
          type: array
          items:
            type: string
          collectionFormat: multi
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Field'
        400:
          $ref: '#/responses/BadRequest'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}/fields/{field_id}:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
      - name: field_id
        in: path
        required: true
        description: id of Field to retrieve
        type: string
        format: uuid
    get:
      summary: List single Field details associated with Field id provided (from set of Fields associated with the organization)
      operationId: fieldGet
      tags: [organization]
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Field'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}/probes:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
    get:
      summary: List all Probes associated with an organization
      operationId: probeList
      tags: [organization]
      parameters: 
        - name: fields
          description: Fields to filter search on
          in: query
          type: array
          items:
            type: string
            format: uuid
          collectionFormat: csv
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Probe'
        400:
          $ref: '#/responses/BadRequest'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}/probes/{probe_id}:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
      - name: probe_id
        in: path
        required: true
        description: id of Probe to retrieve
        type: string
    get:
      summary: List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
      operationId: probeGet
      tags: [organization]
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Probe'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/organization.read
          ApiKeyAuth: []
  
  /organizations/{organization_id}/analytics:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
    get:
      summary: Query reading anlaytics
      description: Queries readings and performs anayltics operations on the them over a sample
      operationId: analyticsQuery
      tags: [telemetry]
      parameters: 
        - name: start_date
          description: Start date and time for the query in RFC3339 format
          in: query
          type: string
          format: date-time
          required: false
        - name: end_date
          description: End date and time for the query in RFC3339 format
          in: query
          type: string
          format: date-time
          required: false
        - name: fields
          description: The fields to return readings for
          in: query
          type: array
          items:
            type: string
            format: uuid
          collectionFormat: csv
        - name: probes
          description: The probes to return readings for
          in: query
          type: array
          items:
            type: string
        - name: properties
          description: The properties to return readings for
          in: query
          type: array
          items:
            type: string
            format: uuid
          collectionFormat: csv
        - name: operation
          description: Operation to perform on reading data
          in: query
          type: string
          enum: [ mean, min, max, first, last, spread, stddev ]
          default: mean
        - name: sample_rate
          description: The operation sample interval, i.e. `15m`
          in: query
          type: string
          default: '15m'
        - name: limit
          description: limit the number of returns per depth
          in: query
          type: integer
          format: int64
        - name: extended
          description: Return extended attributes
          in: query
          type: boolean
          default: false
        - name: sort
          description: return a sorted array, will decrease performance
          in: query
          type: boolean
          default: true
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Reading'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/reading.read
          ApiKeyAuth: []
          
  /organizations/{organization_id}/readings:
    parameters:
      - name: organization_id
        in: path
        required: true
        description: id of Organization to retrieve
        type: string
        format: uuid
    get:
      summary: Query sensor readings
      description: Query sensor readings associated with organization
      operationId: readingsQuery
      tags: [telemetry]
      parameters:
        - name: start_date
          description: |
            Start date and time for the query in RFC3339 format, the default is 24h prior
          in: query
          type: string
          format: date-time
          required: false
        - name: end_date
          description: |
            End date and time for the query in RFC3339 format, the implied default is now
          in: query
          type: string
          format: date-time
          required: false
        - name: fields
          description: The fields to return readings for
          in: query
          type: array
          items:
            type: string
            format: uuid
          collectionFormat: csv
        - name: probes
          description: The probes to return readings for
          in: query
          type: array
          items:
            type: string
          collectionFormat: csv
        - name: properties
          description: The properties to return readings for
          in: query
          type: array
          items:
            type: string
            format: uuid
          collectionFormat: csv
        - name: extended
          description: Return extended attributes
          in: query
          type: boolean
          default: false
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Reading'
        303:
          description: AsyncWait
          headers:
            Location:
              description: The async location to poll for the result
              type: string
        400:
          $ref: '#/responses/BadRequest'
        401:
          $ref: '#/responses/Unauthorized'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/ServerError'
      security:
        - OAuth2:
          - https://api.teralytic.io/reading.read
          ApiKeyAuth: []
          
definitions:
  Organization:
    title: Organization
    description: Organization defines an organization unit in the Teralytic API
    type: object
    properties:
      id:
        type: string
        format: uuid
      name:
        type: string
      slug:
        type: string
      type:
        type: string
        x-nullable: true
        x-go-custom-tag: sql:"column:org_type"
    required:
      - id
      - name
      
  Application:
    title: Application
    description: Application credentials which provide third-party access to organiaztion data
    type: object
    properties:
      id:
        type: string
        format: uuid
        readOnly: true
      organization_id:
        description: The organization this application belongs to
        type: string
        format: uuid
        readOnly: true
      name:
        description: The application name
        type: string
      client_id:
        description: The client id
        type: string
        readOnly: true
      client_secret:
        description: The client secret
        type: string
        readOnly: true
      scope:
        $ref: '#/definitions/StringArray'
      
  Field:
    title: Field
    type: object
    properties:
      id:
        type: string
        format: uuid
      organization_id:
        type: string
        format: uuid
        x-go-custom-tag: sql:"column:belongs_to_org"
      name:
        type: string
      acreage:
        type: number
        x-nullable: true
      crop:
        type: string
      geometry:
        $ref: '#/definitions/FieldGeometry'
        x-go-custom-tag: sql:"column:geo"
    required:
      - id
      - name
      
  FieldGeometry:
    title: FieldGeometry
    type: object
    properties:
      id:
        type: string
      type:
        type: string
        enum: [Feature]
      geometry:
        type: object
        properties:
          type:
            type: string
            enum: [Polygon]
          coordinates:
            type: array
            items:
              type: array
              items:
                type: array
                items:
                  type: number
                  format: float64
                  
  Probe:
    title: Probe
    type: object
    properties:
      id:
        type: string
        format: uuid
      serial_code:
        type: string
      field_id:
        type: string
        format: uuid
        x-nullable: true
        x-go-custom-tag: sql:"column:assoc_with_block"
      organization_id:
        type: string
        format: uuid
        x-nullable: true
        x-go-custom-tag: sql:"column:owner"

  Reading:
    title: Reading
    type: object
    properties:
      id:
        type: string
        description: Base58 encoded identifier for this reading. Corresponds with a single Probe and single timestamp.
        x-nullable: true
      probe_id:
        type: string
        description: id of probe from which Reading was made
        x-nullable: true
      probe_name:
        type: string
        description: Name of probe from which Reading was made
        x-nullable: true
      field_id:
        description: The field id of the probe
        type: string
        format: uuid
        x-nullable: true
      timestamp:
        type: string
        description: Time when Reading was measured
        x-nullable: true
      location:
        description: Location as a single latitude/longitude coordinate (in WGS84 format)
        type: object 
        properties:
          latitude:
            type: number
            format: float64
          longitude:
            type: number
            format: float64
          geohash:
            description: location geohash (12 digit precision)
            type: string
        x-nullable: true
      operation:
        description: The analytics operation
        type: string
        x-nullable: true
      readings:
        type: array
        description: Probe readings
        items:
          $ref: '#/definitions/ReadingData'
        x-omitempty: true
    x-nullable: true
      
  ReadingType:
    description: reading data types
    type: string
    enum: [ SensorData, MicroClimateData, WeatherData ]
      
  ReadingData:
    title: ReadingData
    type: object
    discriminator: type
    properties:
      type: 
        type: string
        enum: [ SensorData, MicroClimateData, WeatherData ]

  SensorData:
    description: Teralytic probe reading
    allOf:
    - $ref: "#/definitions/ReadingData"
    - properties:
        depth:
          description: The depth value
          type: integer
          format: int64
          x-nullable: true
        depth_units:
          description: The depth units (i.e. in, cm)
          type: string
          x-nullable: true
        terascore:
          description: TeraScore
          type: number
          format: float64
          x-nullable: true
        nitrogen:
          description: Nitrogen
          type: number
          format: float64
          x-nullable: true
        nitrogen_ppm:
          description: Nitrogen ppm
          type: number
          format: float64
          x-nullable: true
        phosphorus:
          description: Phosphorus
          type: number
          format: float64
          x-nullable: true
        phosphorus_ppm:
          description: Phosphorus ppm
          type: number
          format: float64
          x-nullable: true
        potassium:
          description: Potassium
          type: number
          format: float64
          x-nullable: true
        potassium_ppm:
          description: Potassium ppm
          type: number
          format: float64
          x-nullable: true
        ec:
          description: Electrical Conductivity
          type: number
          format: float64
          x-nullable: true
        o2:
          description: Dioxygen
          type: number
          format: float64
          x-nullable: true
        pH:
          description: pH Scale
          type: number
          format: float64
          x-nullable: true
        co2:
          description: Carbon Dioxide
          type: number
          format: float64
          x-nullable: true
        awc:
          type: number
          format: float64
          x-nullable: true
        soil_moisture:
          description: Soil Moisture
          type: number
          format: float64
          x-nullable: true
        soil_temperature:
          description: Soil Temperature
          type: number
          format: float64
          x-nullable: true
        soil_texture:
          description: Soil texture type. Data are collected from government data sources (USDA's SSURGO database for fields in the United States)
          type: string
          x-nullable: true
        extended_attributes:
          description: Additional properties
          type: object
          additionalProperties:
            type: object
          x-omitempty: true
  
  MicroClimateData:
    description: Microclimate data near probe
    allOf:
    - $ref: "#/definitions/ReadingData"
    - properties:
        humidity:
          type: number
          format: float64
          x-nullable: true
        irrigation_sched:
          description: Irrigation Schedule
          type: integer
          format: int64
          x-nullable: true
        irrigation_vol_acre_in:
          description: Irrigation volume in3/acre
          type: number
          format: float64
          x-nullable: true
        irrigation_vol_acre_cm:
          description: Irrigation volume c3/acre
          type: number
          format: float64
          x-nullable: true
        lux:
          type: number
          format: float64
          x-nullable: true
        irrigation_gross_in:
          description: Gross irrigation in in3
          type: number
          format: float64
          x-nullable: true
        irrigation_net_in:
          description: Net irrigation in in3
          type: number
          format: float64
          x-nullable: true
        irrigation_gross_cm:
          description: Gross irrigation in cm3
          type: number
          format: float64
          x-nullable: true
        irrigation_net_cm:
          description: Net irrigation in cm3
          type: number
          format: float64
          x-nullable: true
        irrigation_rec:
          description: Irrigation recommendation
          type: string
          x-nullable: true
        temperature:
          description: Air Temperature
          type: number
          format: float64
          x-nullable: true
        eto:
          description: Evaporative Transpiration
          type: number
          format: float64
          x-nullable: true
        irrigation_stream:
          type: number
          format: float64
          x-nullable: true
        organic_matter:
          description: Organic Matter
          type: number
          format: float64
          x-nullable: true
        aw_in6:
          description: available water at 6 inches
          type: number
          format: float
          x-nullable: true
        aw_in18:
          description: available water at 18 inches
          type: number
          format: float
          x-nullable: true
        aw_in36:
          description: available water at 36 inches
          type: number
          format: float
          x-nullable: true
        aw_total:
          description: total available water
          type: number
          format: float
          x-nullable: true
      
  WeatherData:
    description: Current weather data from DarkSky
    allOf:
    - $ref: "#/definitions/ReadingData"
    - properties:
        weather_summary: 
          description: Weather Summary
          type: string
          example: "Mostly Cloudy"
          x-nullable: true
        precip_type:
          description: Precipitation Type
          type: string
          x-nullable: true
        apparent_temp:
          description: Apparent Temperature
          type: number
          format: float64
        cloud_cover:
          description: Cloud Cover
          type: number
          format: float64
          x-nullable: true
        dew_point:
          description: Dew Point
          type: number
          format: float64
          x-nullable: true
        humidity:
          description: Humidity
          type: number
          format: float64
          x-nullable: true
        ozone:
          description: Ozone Rating
          type: number
          format: float64
          x-nullable: true
        precip_intensity:
          description: Precipitation Intesity
          type: number
          format: float64
          x-nullable: true
        precip_probability:
          description: Precipitation Probability
          type: number
          format: float64
          x-nullable: true
        pressure:
          description: Barometric Pressure
          type: number
          format: float64
          x-nullable: true
        temperature:
          description: Temperature
          type: number
          format: float64
          x-nullable: true
        time:
          description: Weather Sample Time (epoch)
          type: number
          format: int64
          x-nullable: true
        uv_index:
          description: UV Index
          type: integer
          x-nullable: true
        wind_bearing:
          description: Wind Bearing
          type: integer
          x-nullable: true
        wind_gust:
          description: Wind Gust
          type: number
          format: float64
          x-nullable: true
        wind_speed:
          description: Wind Speed
          type: number
          format: float64
          x-nullable: true
        visibility:
          description: Visibility
          type: number
          format: float64
          x-nullable: true
  
  User:
    description: A platform user
    properties:
      id:
        description: The users uuid
        type: string
        format: uuid
      organizations:
        $ref: '#/definitions/UUIDArray'
        x-go-custom-tag: sql:"column:belongs_to_orgs"
      first_name:
        description: The user's first name
        type: string
      last_name: 
        description: The user's last name
        type: string
      email:
        description: The user's email address
        type: string
        format: email
      email_verified:
        description: Set if the user's email address is verified
        type: boolean
      username:
        description: The users login name
        type: string
      org_roles:
        type: array
        items:
          $ref: '#/definitions/OrgRole'
        x-omitempty: true
        x-go-custom-tag: gorm:"foreignkey:belongs_to_user"
      property_roles:
        type: array
        items:
          $ref: '#/definitions/PropertyRole'
        x-omitempty: true
        x-go-custom-tag: gorm:"foreignkey:belongs_to_user"
  
  RolePermission:
    description: User role permission
    properties:
      id:
        description: The role id
        type: string
        format: uuid
      domain:
        description: The role domain
        type: string
        enum: [ property, org ]
      action:
        description: The role action
        type: string
        enum: [ export_data, manage_property, manage_properties, manage_team, manage_orders ]
      
  OrgRole:
    description: Organization role mapping
    properties:
      id:
        description: Role id
        type: string
        format: uuid
      user_id:
        description: The user id
        type: string
        format: uuid
        x-go-custom-tag: sql:"column:belongs_to_user"
      organization_id:
        description: The organization id
        type: string
        format: uuid
        x-go-custom-tag: sql:"column:belongs_to_org"
      permission_ids:
        $ref: '#/definitions/UUIDArray'
        x-go-custom-tag: sql:"column:permissions"
      permissions:
        type: array
        items:
          $ref: '#/definitions/RolePermission'
        x-omitempty: true
      owner:
        description: Flag is the user role is an owner
        type: boolean
  
  PropertyRole:
    description: Property role mapping
    properties:
      id:
        description: Role id
        type: string
        format: uuid
      user_id:
        description: The user id
        type: string
        format: uuid
        x-go-custom-tag: sql:"column:belongs_to_user"
      property_id:
        description: The property id
        type: string
        format: uuid
        x-go-custom-tag: sql:"column:belongs_to_property"
      permission_ids:
        $ref: '#/definitions/UUIDArray'
        x-go-custom-tag: sql:"column:permissions"
      permissions:
        type: array
        items:
          $ref: '#/definitions/RolePermission'
        x-omitempty: true
        
  UUIDArray:
    description: array of uuid
    type: array
    items:
      type: string
    x-go-type:
      type: UUIDArray 
      import: 
        package: gitlab.com/ModelRocket/sparks/types
        alias: sparks
    x-omitempty: true
    
  StringArray:
    description: array of strings
    example:
      - read
      - reviewed
    type: array
    items:
      type: string
    x-go-type:
      type: StringArray 
      import: 
        package: gitlab.com/ModelRocket/sparks/types
        alias: sparks
    x-omitempty: true
    
  Error:
    description: Common Error Model
    type: object
    properties:
      code:
        description: The error code
        type: string
        x-nullable: true
      message:
        description: The error message
        type: string
      detail:
        description: The error details
        type: object
        additionalProperties:
          type: string
        x-nullable: true
        x-omitempty: true
    x-go-type:
      type: Error 
      import: 
        package: gitlab.com/ModelRocket/sparks
        alias: sparks
    example:
      message: "something went wrong"
      
responses:
  NotFound:
    description: ResourceNotFound
    schema:
      $ref: "#/definitions/Error"
    examples:
      application/json:
        {
          "message": "object not found"
        }
  Unauthorized:
    description: Unauthorized
    schema:
      $ref: "#/definitions/Error"
    examples:
      application/json:
        {
          "message": "access denied"
        }
  BadRequest:
    description: BadRequest
    schema:
      $ref: "#/definitions/Error"
    examples:
      application/json:
        {
          "message": "invalid parameter"
        }
  ServerError:
    description: ServerError
    schema:
      $ref: "#/definitions/Error"
    examples:
      application/json:
        {
          "message": "internal server error"
        }
/*
 * teralytic
 * The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
 *
 * OpenAPI spec version: 1.0.6
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.teralytic.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.teralytic.model.ReadingData;
import io.teralytic.model.ReadingLocation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Reading
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-01-14T18:59:58.075Z")
public class Reading {
  @SerializedName("id")
  private String id = null;

  @SerializedName("probe_id")
  private String probeId = null;

  @SerializedName("probe_name")
  private String probeName = null;

  @SerializedName("field_id")
  private UUID fieldId = null;

  @SerializedName("timestamp")
  private String timestamp = null;

  @SerializedName("location")
  private ReadingLocation location = null;

  @SerializedName("operation")
  private String operation = null;

  @SerializedName("readings")
  private List<ReadingData> readings = null;

  public Reading id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Base58 encoded identifier for this reading. Corresponds with a single Probe and single timestamp.
   * @return id
  **/
  @ApiModelProperty(value = "Base58 encoded identifier for this reading. Corresponds with a single Probe and single timestamp.")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Reading probeId(String probeId) {
    this.probeId = probeId;
    return this;
  }

   /**
   * id of probe from which Reading was made
   * @return probeId
  **/
  @ApiModelProperty(value = "id of probe from which Reading was made")
  public String getProbeId() {
    return probeId;
  }

  public void setProbeId(String probeId) {
    this.probeId = probeId;
  }

  public Reading probeName(String probeName) {
    this.probeName = probeName;
    return this;
  }

   /**
   * Name of probe from which Reading was made
   * @return probeName
  **/
  @ApiModelProperty(value = "Name of probe from which Reading was made")
  public String getProbeName() {
    return probeName;
  }

  public void setProbeName(String probeName) {
    this.probeName = probeName;
  }

  public Reading fieldId(UUID fieldId) {
    this.fieldId = fieldId;
    return this;
  }

   /**
   * The field id of the probe
   * @return fieldId
  **/
  @ApiModelProperty(value = "The field id of the probe")
  public UUID getFieldId() {
    return fieldId;
  }

  public void setFieldId(UUID fieldId) {
    this.fieldId = fieldId;
  }

  public Reading timestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * Time when Reading was measured
   * @return timestamp
  **/
  @ApiModelProperty(value = "Time when Reading was measured")
  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public Reading location(ReadingLocation location) {
    this.location = location;
    return this;
  }

   /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(value = "")
  public ReadingLocation getLocation() {
    return location;
  }

  public void setLocation(ReadingLocation location) {
    this.location = location;
  }

  public Reading operation(String operation) {
    this.operation = operation;
    return this;
  }

   /**
   * The analytics operation
   * @return operation
  **/
  @ApiModelProperty(value = "The analytics operation")
  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public Reading readings(List<ReadingData> readings) {
    this.readings = readings;
    return this;
  }

  public Reading addReadingsItem(ReadingData readingsItem) {
    if (this.readings == null) {
      this.readings = new ArrayList<ReadingData>();
    }
    this.readings.add(readingsItem);
    return this;
  }

   /**
   * Probe readings
   * @return readings
  **/
  @ApiModelProperty(value = "Probe readings")
  public List<ReadingData> getReadings() {
    return readings;
  }

  public void setReadings(List<ReadingData> readings) {
    this.readings = readings;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reading reading = (Reading) o;
    return Objects.equals(this.id, reading.id) &&
        Objects.equals(this.probeId, reading.probeId) &&
        Objects.equals(this.probeName, reading.probeName) &&
        Objects.equals(this.fieldId, reading.fieldId) &&
        Objects.equals(this.timestamp, reading.timestamp) &&
        Objects.equals(this.location, reading.location) &&
        Objects.equals(this.operation, reading.operation) &&
        Objects.equals(this.readings, reading.readings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, probeId, probeName, fieldId, timestamp, location, operation, readings);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Reading {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    probeId: ").append(toIndentedString(probeId)).append("\n");
    sb.append("    probeName: ").append(toIndentedString(probeName)).append("\n");
    sb.append("    fieldId: ").append(toIndentedString(fieldId)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("    readings: ").append(toIndentedString(readings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


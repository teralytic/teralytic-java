
# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**organizationId** | [**UUID**](UUID.md) | The organization this application belongs to |  [optional]
**name** | **String** | The application name |  [optional]
**clientId** | **String** | The client id |  [optional]
**clientSecret** | **String** | The client secret |  [optional]
**scope** | [**StringArray**](StringArray.md) |  |  [optional]





# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | The error code |  [optional]
**message** | **String** | The error message |  [optional]
**detail** | **Map&lt;String, String&gt;** | The error details |  [optional]




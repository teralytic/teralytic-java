
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) | The users uuid |  [optional]
**organizations** | [**UUIDArray**](UUIDArray.md) |  |  [optional]
**firstName** | **String** | The user&#39;s first name |  [optional]
**lastName** | **String** | The user&#39;s last name |  [optional]
**email** | **String** | The user&#39;s email address |  [optional]
**emailVerified** | **Boolean** | Set if the user&#39;s email address is verified |  [optional]
**username** | **String** | The users login name |  [optional]
**orgRoles** | [**List&lt;OrgRole&gt;**](OrgRole.md) |  |  [optional]
**propertyRoles** | [**List&lt;PropertyRole&gt;**](PropertyRole.md) |  |  [optional]




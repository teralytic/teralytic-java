# TelemetryApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**analyticsQuery**](TelemetryApi.md#analyticsQuery) | **GET** /organizations/{organization_id}/analytics | Query reading anlaytics
[**readingsQuery**](TelemetryApi.md#readingsQuery) | **GET** /organizations/{organization_id}/readings | Query sensor readings


<a name="analyticsQuery"></a>
# **analyticsQuery**
> List&lt;Reading&gt; analyticsQuery(organizationId, startDate, endDate, fields, probes, properties, operation, sampleRate, limit, extended, sort)

Query reading anlaytics

Queries readings and performs anayltics operations on the them over a sample

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.TelemetryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

TelemetryApi apiInstance = new TelemetryApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
OffsetDateTime startDate = OffsetDateTime.now(); // OffsetDateTime | Start date and time for the query in RFC3339 format
OffsetDateTime endDate = OffsetDateTime.now(); // OffsetDateTime | End date and time for the query in RFC3339 format
List<UUID> fields = Arrays.asList(new UUID()); // List<UUID> | The fields to return readings for
List<String> probes = Arrays.asList("probes_example"); // List<String> | The probes to return readings for
List<UUID> properties = Arrays.asList(new UUID()); // List<UUID> | The properties to return readings for
String operation = "mean"; // String | Operation to perform on reading data
String sampleRate = "15m"; // String | The operation sample interval, i.e. `15m`
Long limit = 789L; // Long | limit the number of returns per depth
Boolean extended = false; // Boolean | Return extended attributes
Boolean sort = true; // Boolean | return a sorted array, will decrease performance
try {
    List<Reading> result = apiInstance.analyticsQuery(organizationId, startDate, endDate, fields, probes, properties, operation, sampleRate, limit, extended, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TelemetryApi#analyticsQuery");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **startDate** | **OffsetDateTime**| Start date and time for the query in RFC3339 format | [optional]
 **endDate** | **OffsetDateTime**| End date and time for the query in RFC3339 format | [optional]
 **fields** | [**List&lt;UUID&gt;**](UUID.md)| The fields to return readings for | [optional]
 **probes** | [**List&lt;String&gt;**](String.md)| The probes to return readings for | [optional]
 **properties** | [**List&lt;UUID&gt;**](UUID.md)| The properties to return readings for | [optional]
 **operation** | **String**| Operation to perform on reading data | [optional] [default to mean] [enum: mean, min, max, first, last, spread, stddev]
 **sampleRate** | **String**| The operation sample interval, i.e. &#x60;15m&#x60; | [optional] [default to 15m]
 **limit** | **Long**| limit the number of returns per depth | [optional]
 **extended** | **Boolean**| Return extended attributes | [optional] [default to false]
 **sort** | **Boolean**| return a sorted array, will decrease performance | [optional] [default to true]

### Return type

[**List&lt;Reading&gt;**](Reading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="readingsQuery"></a>
# **readingsQuery**
> List&lt;Reading&gt; readingsQuery(organizationId, startDate, endDate, fields, probes, properties, extended)

Query sensor readings

Query sensor readings associated with organization

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.TelemetryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

TelemetryApi apiInstance = new TelemetryApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
OffsetDateTime startDate = OffsetDateTime.now(); // OffsetDateTime | Start date and time for the query in RFC3339 format, the default is 24h prior 
OffsetDateTime endDate = OffsetDateTime.now(); // OffsetDateTime | End date and time for the query in RFC3339 format, the implied default is now 
List<UUID> fields = Arrays.asList(new UUID()); // List<UUID> | The fields to return readings for
List<String> probes = Arrays.asList("probes_example"); // List<String> | The probes to return readings for
List<UUID> properties = Arrays.asList(new UUID()); // List<UUID> | The properties to return readings for
Boolean extended = false; // Boolean | Return extended attributes
try {
    List<Reading> result = apiInstance.readingsQuery(organizationId, startDate, endDate, fields, probes, properties, extended);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TelemetryApi#readingsQuery");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **startDate** | **OffsetDateTime**| Start date and time for the query in RFC3339 format, the default is 24h prior  | [optional]
 **endDate** | **OffsetDateTime**| End date and time for the query in RFC3339 format, the implied default is now  | [optional]
 **fields** | [**List&lt;UUID&gt;**](UUID.md)| The fields to return readings for | [optional]
 **probes** | [**List&lt;String&gt;**](String.md)| The probes to return readings for | [optional]
 **properties** | [**List&lt;UUID&gt;**](UUID.md)| The properties to return readings for | [optional]
 **extended** | **Boolean**| Return extended attributes | [optional] [default to false]

### Return type

[**List&lt;Reading&gt;**](Reading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


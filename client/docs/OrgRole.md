
# OrgRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) | Role id |  [optional]
**userId** | [**UUID**](UUID.md) | The user id |  [optional]
**organizationId** | [**UUID**](UUID.md) | The organization id |  [optional]
**permissionIds** | [**UUIDArray**](UUIDArray.md) |  |  [optional]
**permissions** | [**List&lt;RolePermission&gt;**](RolePermission.md) |  |  [optional]
**owner** | **Boolean** | Flag is the user role is an owner |  [optional]




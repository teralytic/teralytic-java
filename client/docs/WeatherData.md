
# WeatherData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weatherSummary** | **String** | Weather Summary |  [optional]
**precipType** | **String** | Precipitation Type |  [optional]
**apparentTemp** | [**BigDecimal**](BigDecimal.md) | Apparent Temperature |  [optional]
**cloudCover** | [**BigDecimal**](BigDecimal.md) | Cloud Cover |  [optional]
**dewPoint** | [**BigDecimal**](BigDecimal.md) | Dew Point |  [optional]
**humidity** | [**BigDecimal**](BigDecimal.md) | Humidity |  [optional]
**ozone** | [**BigDecimal**](BigDecimal.md) | Ozone Rating |  [optional]
**precipIntensity** | [**BigDecimal**](BigDecimal.md) | Precipitation Intesity |  [optional]
**precipProbability** | [**BigDecimal**](BigDecimal.md) | Precipitation Probability |  [optional]
**pressure** | [**BigDecimal**](BigDecimal.md) | Barometric Pressure |  [optional]
**temperature** | [**BigDecimal**](BigDecimal.md) | Temperature |  [optional]
**time** | [**BigDecimal**](BigDecimal.md) | Weather Sample Time (epoch) |  [optional]
**uvIndex** | **Integer** | UV Index |  [optional]
**windBearing** | **Integer** | Wind Bearing |  [optional]
**windGust** | [**BigDecimal**](BigDecimal.md) | Wind Gust |  [optional]
**windSpeed** | [**BigDecimal**](BigDecimal.md) | Wind Speed |  [optional]
**visibility** | [**BigDecimal**](BigDecimal.md) | Visibility |  [optional]




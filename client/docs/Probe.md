
# Probe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**serialCode** | **String** |  |  [optional]
**fieldId** | [**UUID**](UUID.md) |  |  [optional]
**organizationId** | [**UUID**](UUID.md) |  |  [optional]




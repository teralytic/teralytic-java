
# ReadingLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**longitude** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**geohash** | **String** | location geohash (12 digit precision) |  [optional]




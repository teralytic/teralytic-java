
# SensorData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depth** | **Long** | The depth value |  [optional]
**depthUnits** | **String** | The depth units (i.e. in, cm) |  [optional]
**terascore** | [**BigDecimal**](BigDecimal.md) | TeraScore |  [optional]
**nitrogen** | [**BigDecimal**](BigDecimal.md) | Nitrogen |  [optional]
**nitrogenPpm** | [**BigDecimal**](BigDecimal.md) | Nitrogen ppm |  [optional]
**phosphorus** | [**BigDecimal**](BigDecimal.md) | Phosphorus |  [optional]
**phosphorusPpm** | [**BigDecimal**](BigDecimal.md) | Phosphorus ppm |  [optional]
**potassium** | [**BigDecimal**](BigDecimal.md) | Potassium |  [optional]
**potassiumPpm** | [**BigDecimal**](BigDecimal.md) | Potassium ppm |  [optional]
**ec** | [**BigDecimal**](BigDecimal.md) | Electrical Conductivity |  [optional]
**o2** | [**BigDecimal**](BigDecimal.md) | Dioxygen |  [optional]
**pH** | [**BigDecimal**](BigDecimal.md) | pH Scale |  [optional]
**co2** | [**BigDecimal**](BigDecimal.md) | Carbon Dioxide |  [optional]
**awc** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**soilMoisture** | [**BigDecimal**](BigDecimal.md) | Soil Moisture |  [optional]
**soilTemperature** | [**BigDecimal**](BigDecimal.md) | Soil Temperature |  [optional]
**soilTexture** | **String** | Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States) |  [optional]
**extendedAttributes** | **Map&lt;String, Object&gt;** | Additional properties |  [optional]




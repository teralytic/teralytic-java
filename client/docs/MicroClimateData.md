
# MicroClimateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**humidity** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**irrigationSched** | **Long** | Irrigation Schedule |  [optional]
**irrigationVolAcreIn** | [**BigDecimal**](BigDecimal.md) | Irrigation volume in3/acre |  [optional]
**irrigationVolAcreCm** | [**BigDecimal**](BigDecimal.md) | Irrigation volume c3/acre |  [optional]
**lux** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**irrigationGrossIn** | [**BigDecimal**](BigDecimal.md) | Gross irrigation in in3 |  [optional]
**irrigationNetIn** | [**BigDecimal**](BigDecimal.md) | Net irrigation in in3 |  [optional]
**irrigationGrossCm** | [**BigDecimal**](BigDecimal.md) | Gross irrigation in cm3 |  [optional]
**irrigationNetCm** | [**BigDecimal**](BigDecimal.md) | Net irrigation in cm3 |  [optional]
**irrigationRec** | **String** | Irrigation recommendation |  [optional]
**temperature** | [**BigDecimal**](BigDecimal.md) | Air Temperature |  [optional]
**eto** | [**BigDecimal**](BigDecimal.md) | Evaporative Transpiration |  [optional]
**irrigationStream** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**organicMatter** | [**BigDecimal**](BigDecimal.md) | Organic Matter |  [optional]
**awIn6** | **Float** | available water at 6 inches |  [optional]
**awIn18** | **Float** | available water at 18 inches |  [optional]
**awIn36** | **Float** | available water at 36 inches |  [optional]
**awTotal** | **Float** | total available water |  [optional]




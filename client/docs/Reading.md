
# Reading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Base58 encoded identifier for this reading. Corresponds with a single Probe and single timestamp. |  [optional]
**probeId** | **String** | id of probe from which Reading was made |  [optional]
**probeName** | **String** | Name of probe from which Reading was made |  [optional]
**fieldId** | [**UUID**](UUID.md) | The field id of the probe |  [optional]
**timestamp** | **String** | Time when Reading was measured |  [optional]
**location** | [**ReadingLocation**](ReadingLocation.md) |  |  [optional]
**operation** | **String** | The analytics operation |  [optional]
**readings** | [**List&lt;ReadingData&gt;**](ReadingData.md) | Probe readings |  [optional]





# FieldGeometryGeometry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**coordinates** | [**List&lt;List&lt;List&lt;BigDecimal&gt;&gt;&gt;**](List.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
POLYGON | &quot;Polygon&quot;




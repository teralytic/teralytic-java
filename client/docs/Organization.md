
# Organization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  | 
**name** | **String** |  | 
**slug** | **String** |  |  [optional]
**type** | **String** |  |  [optional]




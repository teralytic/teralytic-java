
# RolePermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) | The role id |  [optional]
**domain** | [**DomainEnum**](#DomainEnum) | The role domain |  [optional]
**action** | [**ActionEnum**](#ActionEnum) | The role action |  [optional]


<a name="DomainEnum"></a>
## Enum: DomainEnum
Name | Value
---- | -----
PROPERTY | &quot;property&quot;
ORG | &quot;org&quot;


<a name="ActionEnum"></a>
## Enum: ActionEnum
Name | Value
---- | -----
EXPORT_DATA | &quot;export_data&quot;
MANAGE_PROPERTY | &quot;manage_property&quot;
MANAGE_PROPERTIES | &quot;manage_properties&quot;
MANAGE_TEAM | &quot;manage_team&quot;
MANAGE_ORDERS | &quot;manage_orders&quot;




# OrganizationApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applicationCreate**](OrganizationApi.md#applicationCreate) | **POST** /organizations/{organization_id}/applications | Create a new organization client with the specified scope
[**applicationDelete**](OrganizationApi.md#applicationDelete) | **DELETE** /organizations/{organization_id}/applications/{application_id} | Delete an application api client
[**applicationList**](OrganizationApi.md#applicationList) | **GET** /organizations/{organization_id}/applications | Get organization applications
[**fieldGet**](OrganizationApi.md#fieldGet) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the organization)
[**fieldList**](OrganizationApi.md#fieldList) | **GET** /organizations/{organization_id}/fields | List all Fields associated with an organization
[**organizationGet**](OrganizationApi.md#organizationGet) | **GET** /organizations/{organization_id} | Get a specific organization
[**organizationList**](OrganizationApi.md#organizationList) | **GET** /organizations | List all Organizations
[**probeGet**](OrganizationApi.md#probeGet) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
[**probeList**](OrganizationApi.md#probeList) | **GET** /organizations/{organization_id}/probes | List all Probes associated with an organization


<a name="applicationCreate"></a>
# **applicationCreate**
> Application applicationCreate(organizationId, application)

Create a new organization client with the specified scope

Create a new third-party application client

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization for the operation
Application application = new Application(); // Application | The application to create
try {
    Application result = apiInstance.applicationCreate(organizationId, application);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#applicationCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization for the operation |
 **application** | [**Application**](Application.md)| The application to create | [optional]

### Return type

[**Application**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="applicationDelete"></a>
# **applicationDelete**
> applicationDelete(organizationId, applicationId)

Delete an application api client

Delete an application client for the organization from the database

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization for the operation
UUID applicationId = new UUID(); // UUID | id of application to delete
try {
    apiInstance.applicationDelete(organizationId, applicationId);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#applicationDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization for the operation |
 **applicationId** | [**UUID**](.md)| id of application to delete |

### Return type

null (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="applicationList"></a>
# **applicationList**
> List&lt;Application&gt; applicationList(organizationId)

Get organization applications

List all applications for an organization

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization for the operation
try {
    List<Application> result = apiInstance.applicationList(organizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#applicationList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization for the operation |

### Return type

[**List&lt;Application&gt;**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="fieldGet"></a>
# **fieldGet**
> Field fieldGet(organizationId, fieldId)

List single Field details associated with Field id provided (from set of Fields associated with the organization)

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
UUID fieldId = new UUID(); // UUID | id of Field to retrieve
try {
    Field result = apiInstance.fieldGet(organizationId, fieldId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#fieldGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **fieldId** | [**UUID**](.md)| id of Field to retrieve |

### Return type

[**Field**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="fieldList"></a>
# **fieldList**
> List&lt;Field&gt; fieldList(organizationId, points)

List all Fields associated with an organization

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
List<String> points = Arrays.asList("points_example"); // List<String> | Array of points (lat,lng) describing a polygon search pattern.
try {
    List<Field> result = apiInstance.fieldList(organizationId, points);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#fieldList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **points** | [**List&lt;String&gt;**](String.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional]

### Return type

[**List&lt;Field&gt;**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organizationGet"></a>
# **organizationGet**
> Organization organizationGet(organizationId)

Get a specific organization

List single Organization details associated with Organization id provided

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
try {
    Organization result = apiInstance.organizationGet(organizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |

### Return type

[**Organization**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organizationList"></a>
# **organizationList**
> List&lt;Organization&gt; organizationList()

List all Organizations

Lists all organization, requires admin scope

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
try {
    List<Organization> result = apiInstance.organizationList();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationList");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Organization&gt;**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="probeGet"></a>
# **probeGet**
> Probe probeGet(organizationId, probeId)

List single Probe details associated with Probe id provided (from set of Fields associated with the account key)

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
String probeId = "probeId_example"; // String | id of Probe to retrieve
try {
    Probe result = apiInstance.probeGet(organizationId, probeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#probeGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **probeId** | **String**| id of Probe to retrieve |

### Return type

[**Probe**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="probeList"></a>
# **probeList**
> List&lt;Probe&gt; probeList(organizationId, fields)

List all Probes associated with an organization

### Example
```java
// Import classes:
//import io.teralytic.ApiClient;
//import io.teralytic.ApiException;
//import io.teralytic.Configuration;
//import io.teralytic.auth.*;
//import io.teralytic.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: ApiKeyAuth
ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
ApiKeyAuth.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.setApiKeyPrefix("Token");

// Configure OAuth2 access token for authorization: OAuth2
OAuth OAuth2 = (OAuth) defaultClient.getAuthentication("OAuth2");
OAuth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
UUID organizationId = new UUID(); // UUID | id of Organization to retrieve
List<UUID> fields = Arrays.asList(new UUID()); // List<UUID> | Fields to filter search on
try {
    List<Probe> result = apiInstance.probeList(organizationId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#probeList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**UUID**](.md)| id of Organization to retrieve |
 **fields** | [**List&lt;UUID&gt;**](UUID.md)| Fields to filter search on | [optional]

### Return type

[**List&lt;Probe&gt;**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


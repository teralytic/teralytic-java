
# PropertyRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) | Role id |  [optional]
**userId** | [**UUID**](UUID.md) | The user id |  [optional]
**propertyId** | [**UUID**](UUID.md) | The property id |  [optional]
**permissionIds** | [**UUIDArray**](UUIDArray.md) |  |  [optional]
**permissions** | [**List&lt;RolePermission&gt;**](RolePermission.md) |  |  [optional]




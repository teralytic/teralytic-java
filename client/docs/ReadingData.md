
# ReadingData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
SENSORDATA | &quot;SensorData&quot;
MICROCLIMATEDATA | &quot;MicroClimateData&quot;
WEATHERDATA | &quot;WeatherData&quot;




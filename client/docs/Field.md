
# Field

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  | 
**organizationId** | [**UUID**](UUID.md) |  |  [optional]
**name** | **String** |  | 
**acreage** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**crop** | **String** |  |  [optional]
**geometry** | [**FieldGeometry**](FieldGeometry.md) |  |  [optional]




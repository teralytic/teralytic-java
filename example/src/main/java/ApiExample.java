
import com.google.api.client.auth.oauth2.ClientCredentialsTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import io.teralytic.ApiClient;
import io.teralytic.ApiException;
import io.teralytic.api.OrganizationApi;
import io.teralytic.model.Organization;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rodriguise
 */
public class ApiExample {
    static ApiClient apiClient;
    static OrganizationApi api;
    static List<Organization> orgs;
    
    static String requestAccessToken() throws IOException {
        TokenResponse response = new ClientCredentialsTokenRequest(new NetHttpTransport(), new JacksonFactory(),
                new GenericUrl("https://auth.teralytic.io/token"))
                .setClientAuthentication(
                        new BasicAuthentication("<<client-id>>", "<<client-secret>>")).execute();
        
        return response.getAccessToken();
    }

    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getAnonymousLogger();
        String token = requestAccessToken();
       
        apiClient = new ApiClient();
        apiClient.addDefaultHeader("x-api-key", "<<api-key>>");
        apiClient.setAccessToken(token);
        
        api = new OrganizationApi(apiClient);
           
        try {
            orgs = api.organizationList();
            
            if (orgs != null) {
                logger.log(Level.INFO, orgs.toString());
            } else {

                logger.log(Level.WARNING, "No orgnaizations");
            }
        
        } catch (ApiException e) {
            logger.log(Level.SEVERE, "an exception was thrown", e);
        }
         
    }
}

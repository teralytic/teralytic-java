VERSION := `npm view @modelrocket/mvault version`
GITLAB_PROJECT := 8558474
SWAGGER_CODEGEN ?= docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli:2.4.0
SWAGGER_SPEC ?= swagger.yaml
GRUNT_REL ?= prerelease
BUMP ?= yes

default: publish

bump:
ifeq ($(BUMP),yes)
	grunt bump-only:$(GRUNT_REL)
endif

fetch: bump
	curl -o swagger.yaml --header 'PRIVATE-TOKEN: $(GITLAB_TOKEN)' https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT)/repository/files/api%2Fteralytic.yaml/raw?ref=master

client: fetch
	$(SWAGGER_CODEGEN) generate -l java -i /local/$(SWAGGER_SPEC) -c /local/config.json -o /local/client

commit: client
ifeq ($(BUMP),yes)
	grunt bump-commit
endif

publish: commit
	cd client && mvn install

.PHONY: \
	client
